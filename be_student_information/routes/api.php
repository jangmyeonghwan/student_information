<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Be_student_informationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('post', [Be_student_informationController::class, 'post_article']);
Route::get('fetch_all', [Be_student_informationController::class, 'fetch_all_articles']);
Route::post('update', [Be_student_informationController::class, 'update_article']);
Route::post('delete', [Be_student_informationController::class, 'delete_article']);

Route::post('search_student_id_number', [Be_student_informationController::class, 'search_student_id_number']);
Route::post('search_first_name', [Be_student_informationController::class, 'search_first_name']);
Route::post('search_middle_name', [Be_student_informationController::class, 'search_middle_name']);
Route::post('search_last_name', [Be_student_informationController::class, 'search_last_name']);
Route::post('search_course', [Be_student_informationController::class, 'search_course']);
Route::post('search_section', [Be_student_informationController::class, 'search_section']);
