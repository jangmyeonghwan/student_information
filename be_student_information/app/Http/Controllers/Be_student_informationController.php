<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\Be_student_information;

class Be_student_informationController extends Controller
{

    // CREATE FUNCTION
    public function post_article(Request $req){

        DB::beginTransaction();

            $valid = Validator::make($req->all(), [
                'student_id_number' => 'required|string|unique:be_student_informations|max:255'
            ]);

            if ($valid->fails()) {
                return response()->json([
                    'errors'    =>  $valid->errors()
                ],400);
            }

            try{

                $be_student_information = Be_student_information::create([
                    "student_id_number" => $req->student_id_number,
                    "first_name" => $req->first_name,
                    "middle_name" => $req->middle_name,
                    "last_name" => $req->last_name,
                    "course" => $req->course,
                    "section" => $req->section,
                    "created_by" => $req->created_by,
                    "updated_by" => $req->updated_by
                ]);

                DB::commit();

                return response()->json([
                    'message' => "Article has been posted successfully!"
                ]);

            }catch(Exception $e){

                DB::rollback();

                return response()->json([
                    'errors'    =>  [ 'Can`t create your entry as of now. Contact the developer to fix it. Error Code : Be_student_information-0x01' ],
                    'msg'   =>  $e->getMessage()
                ],500);

            }

    }

    public function fetch_all_articles(){

        if(Be_student_information::count() == 0){

            return response()->json([
                'message'   =>  "No record found."
            ]);

        }else{

            return response()->json([
                'data'  =>  Be_student_information::all()
            ]);

        }

    }

    public function update_article(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'student_id_number' => 'required|string|unique:be_student_informations|max:255'
        ]);

        if ($valid->fails()) {
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }

        try{

            $be_student_informations = Be_student_information::where('id', $req->id)->update([
                    "student_id_number" => $req->student_id_number,
                    "first_name" => $req->first_name,
                    "middle_name" => $req->middle_name,
                    "last_name" => $req->last_name,
                    "course" => $req->course,
                    "section" => $req->section,
                    "updated_by" => $req->updated_by
            ]);

            if($be_student_informations == 0){

                DB::rollback();

                return response()->json([
                    'message'    =>  'Article ID does not exists.'
                ],400);

            }else{

                DB::commit();

                return response()->json([
                    'message' => "Article has been successfully updated!"
                ]);

            }

        }catch(Exception $e){

            DB::rollback();

            return response()->json([
                'errors'    =>  [ 'Can`t create your entry as of now. Contact the developer to fix it. Error Code : Be_student_information-0x02' ],
                'msg'   =>  $e->getMessage()
            ],500);

        }

    }

    public function delete_article(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'id' => 'required|integer'
        ]);

        if ($valid->fails()) {
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }

        try{

            $delete = Be_student_information::findOrFail($req->id);
            $delete->delete();
            DB::commit();

            return response()->json([
                'text'  =>  'Accounting Dimension has been deleted.',

            ]);

        }catch(Exception $e){

            DB::rollback();

            return response()->json([
                'errors'    =>  [ 'Can`t create your entry as of now. Contact the developer to fix it. Error Code : Be_student_information-0x03' ],
                'msg'   =>  $e->getMessage()
            ],500);

        }

    }

    // Search student_id_number
    public function search_student_id_number(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'student_id_number' => 'required|string|unique:be_student_informations|max:255'
        ]);


        if(Be_student_information::count() == 0){

            return response()->json([
                'message'   =>  "No record found."
            ]);

        }else{

            return response()->json([
                'data'  =>  Be_student_information::where('student_id_number', $req->student_id_number)->get()
            ]);

        }

    }

    // Search first name
    public function search_first_name(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'student_id_number' => 'required|string|unique:be_student_informations|max:255'
        ]);


        if(Be_student_information::count() == 0){

            return response()->json([
                'message'   =>  "No record found."
            ]);

        }else{

            return response()->json([
                'data'  =>  Be_student_information::where('first_name', $req->first_name)->get()
            ]);

        }

    }

    // Search middle name
    public function search_middle_name(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'student_id_number' => 'required|string|unique:be_student_informations|max:255'
        ]);


        if(Be_student_information::count() == 0){

            return response()->json([
                'message'   =>  "No record found."
            ]);

        }else{

            return response()->json([
                'data'  =>  Be_student_information::where('middle_name', $req->middle_name)->get()
            ]);

        }

    }

    // Search last name
    public function search_last_name(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'student_id_number' => 'required|string|unique:be_student_informations|max:255'
        ]);


        if(Be_student_information::count() == 0){

            return response()->json([
                'message'   =>  "No record found."
            ]);

        }else{

            return response()->json([
                'data'  =>  Be_student_information::where('last_name', $req->last_name)->get()
            ]);

        }

    }

    // Search course
    public function search_course(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'student_id_number' => 'required|string|unique:be_student_informations|max:255'
        ]);


        if(Be_student_information::count() == 0){

            return response()->json([
                'message'   =>  "No record found."
            ]);

        }else{

            return response()->json([
                'data'  =>  Be_student_information::where('course', $req->course)->get()
            ]);

        }

    }

    // Search section
    public function search_section(Request $req){

        DB::beginTransaction();

        $valid = Validator::make($req->all(), [
            'student_id_number' => 'required|string|unique:be_student_informations|max:255'
        ]);


        if(Be_student_information::count() == 0){

            return response()->json([
                'message'   =>  "No record found."
            ]);

        }else{

            return response()->json([
                'data'  =>  Be_student_information::where('section', $req->section)->get()
            ]);

        }

    }



}
