<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Be_student_information extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id_number',
        'first_name',
        'middle_name',
        'last_name',
        'course',
        'section',
        'created_by',
        'updated_by'
    ];
}
